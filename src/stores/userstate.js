import { writable, derived } from 'svelte/store';

const lang = "en_US";

function createToken() {
  const { subscribe, set, update } = writable(localStorage.getItem('token'));

  return {
    subscribe,
    set: (token) => {localStorage.setItem('token', token); set(token)}
  }
}

export const token = createToken();
const claims = derived(token,
  $token => parseJwt($token)
);

export const userstate = {
  subscribe: claims.subscribe,
  set: token.set
};

function parseJwt (token) {
  if (!token || token == undefined) {
    return {}
  }
  var base64Url = token.split('.')[1]
  if (!base64Url || base64Url == undefined) {
    return {}
  }
  var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/')
  var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
  }).join(''))

  return JSON.parse(jsonPayload)
}
