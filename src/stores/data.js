import { writable } from 'svelte/store';
import { token } from './userstate.js';

const lang = "en_US";
var t = undefined;
token.subscribe((v) => {t = v});

function createTest() {
  const { subscribe, set, update } = writable(null);

  async function get(addr) {
    console.log(t)
    await fetch(addr, {
        headers: {
          'Authorization': "Bearer " + t
        }
      })
      .then(r => r.json())
      .then(data => {
        set(data);
      });
  }

  return {
    subscribe,
    set,
    get
  }
}

export const logEntries = createTest();

window.onload = (event) => {
  logEntries.get(`https://log.brurberg.no/api/all`)
};
